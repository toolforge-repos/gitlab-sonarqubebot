<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GitlabController extends Controller
{
    private const FEEDBACK_URL =
        'https://www.mediawiki.org/wiki/Talk:Continuous_integration/Codehealth_Pipeline';

    private const SONARQUBE_HOST = 'https://sonarcloud.io';

    public function processAnalysis(Request $request): string
    {
        if ($request->isMethod('GET')) {
            return redirect()->away(self::FEEDBACK_URL);
        }

        $hmac = $request->headers->get('X-Sonar-Webhook-Hmac-Sha256');

        if (! $hmac || $hmac !== hash_hmac('sha256', $request->getContent(), env('SONARQUBE_HMAC'))) {
            Log::info('HMAC validation error.');

            return response('HMAC validation error', 500);
        }

        $analysisJson = json_decode($request->getContent(), true);

        $gitlabProject = $analysisJson['properties']['sonar.analysis.COMMIT_SHA']
            ?? $analysisJson['properties']['sonar.analysis.commitSHA']
            ?? false;
        if (! $gitlabProject) {
            /*
             *NOT A GITLAB PROJECT
             * */
            return response('', 200);
        }

        if ($analysisJson['branch']['name'] === 'main') {
            // Skip commenting on main for now.
            Log::info('Main Branch');

            return response('', 200);
        }

        //Get coverage values via api

        $key = $analysisJson['project']['key'] ?? '';

        $apiRequest = Http::get(self::SONARQUBE_HOST.'/api/measures/component?component='.$key.'&branch='.$analysisJson['branch']['name'].'&metricKeys=coverage');
        if ($apiRequest->successful()) {
            $apiResponse = json_decode($apiRequest->body(), true);
            $coverage = isset($apiResponse['component']['measures'][0]['value'])
                ? ' ('.$apiResponse['component']['measures'][0]['value'].'% Estimated after merge)'
                : '';
        }

        $passedQualityGate = $analysisJson['qualityGate']['status'] === 'OK';
        $successMessage = $passedQualityGate ?
            '✅ Quality gate passed!' :
            '❌ Quality gate failed';
        $detailsMessage = '';
        foreach ($analysisJson['qualityGate']['conditions']  as $condition) {
            $humanReadableReason = '';
            $humanReadableMetric = trim(str_replace('new', '', str_replace('_', ' ',
                $condition['metric'])));
            if (isset($condition['value'])) {
                $humanReadableReason = $condition['value'].' is '.strtolower(str_replace('_',
                    ' ', $condition['operator'])).' '.$condition['errorThreshold'];
            }
            $detailsMessage .= ($condition['status'] === 'OK' || $condition['status'] === 'NO_VALUE') ?
                "\n* ✔ ".$humanReadableMetric.($humanReadableMetric == 'coverage' ? ($coverage ?? '') : '') :
                "\n* ❌ ".$humanReadableMetric.' ('.$humanReadableReason.')';
        }
        $detailsMessage .= "\n\nReport: ".$analysisJson['branch']['url'];
        if (! $passedQualityGate) {
            $detailsMessage .= "\n\nThis patch can still be merged.";
        }

        $detailsMessage .= "\n\n Please give feedback and report false positives at ".self::FEEDBACK_URL;
        $gitlabComment = $successMessage."\n\n".$detailsMessage;

        $projectId = $analysisJson['properties']['sonar.analysis.PROJECT_ID'] ?? '';
        $commitSha = $analysisJson['properties']['sonar.analysis.COMMIT_SHA'] ?? '';

        $response = Http::withHeaders([
            'PRIVATE-TOKEN' => env('PRIVATE_TOKEN'),
        ])->withUrlParameters([
            'endpoint' => 'https://gitlab.wikimedia.org/api/v4',
            'project' => $projectId,
            'sha' => $commitSha,
        ])->post('{+endpoint}/projects/{project}/repository/commits/{sha}/comments', [
            'note' => $gitlabComment,
        ]);

        if ($response->status() !== 201) {
            Log::warning('Gitlab status: '.$response->status().' Message: '.$response->body());
        } else {
            Log::info('Gitlab status: '.$response->status().' Message: '.$response->body());
        }

        return response('', 200);
    }
}

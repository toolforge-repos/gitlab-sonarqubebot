## About SonarQube Bot

Receive SonarQube webhook data and post reviews to [Gitab](https://gitlab.wikimedia.org/).

This project is hosted at https://tools.wmflabs.org/gitlab-sonarqubebot/ where it listens for webhook data from SonarQube. It then posts a review to Gitlab based on the quality gate success/failure, with verification +1 given for passing quality gate checks. Robot comments are created with each issue found.
## License

[MIT license](https://opensource.org/licenses/MIT).

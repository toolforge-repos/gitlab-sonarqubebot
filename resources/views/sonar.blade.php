<!DOCTYPE html>
<html lang="en">
<head>
    <title>Pull Request Decoration</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        .decoration {
            margin: 0 auto;
            width: 600px;
            border: 1px solid #ddd;
            padding: 10px;
            border-radius: 4px;
        }

        .decoration h2 {
            color: #333;
        }

        .decoration .stats {
            display: flex;
            justify-content: space-between;
        }

        .decoration .stats .stat {
            margin: 10px;
        }

        .decoration .stats .stat h4 {
            margin: 0;
            color: #666;
        }

        .decoration .stats .stat span {
            font-size: 24px;
        }
    </style>
</head>
<body>
<div class="decoration">
    <h2>Pull Request Decoration - SonarQube</h2>
    <h4>This is a dummy test page to validate the application is working!</h4>
    <div class="stats">
        <div class="stat">
            <h4>Code Quality</h4>
            <span>A+</span>
        </div>
        <div class="stat">
            <h4>Bugs</h4>
            <span>0</span>
        </div>
        <div class="stat">
            <h4>Vulnerabilities</h4>
            <span>1</span>
        </div>
    </div>
</div>
</body>
</html>
